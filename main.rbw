require 'gosu'
require_relative 'config'
require_relative 'src/render'
require_relative 'src/grid'
require_relative 'src/seeds'
require_relative 'src/input'

class MyWindow < Gosu::Window
  def initialize
    super 1280, 720
    self.caption = 'Ruby Game of Life'
    @font = Gosu::Font.new(self, Gosu::default_font_name, 20)

    @grid = Grid.new(Config::GRID_WIDTH, Config::GRID_HEIGHT)
    @input = Input.new
    @state = :pause

    load
  end

  def load
    @grid.create
    @grid.set_neighbours
  end

  def update
    if @state == :play
      @grid.propagate
      @grid.shift
    end
  end

  def draw
    Render::world(@grid.map, @grid.history, @state)

    @font.draw_text("#{@state.upcase} [space]", 1280/2-80, 682, 1, 1.0, 1.0, Gosu::Color::WHITE)
  end

  def button_down(id)
    case id
      when Gosu::MsLeft
        @input.update_mouse_position(mouse_x, mouse_y) if @state == :pause
        if @state == :pause && @input.within_grid?
          @grid.seed(@input.calculate_grid_row, @input.calculate_grid_column)
        end
      when Gosu::KbSpace
        @state = @state == :play ? :pause : :play
      when Gosu::KbR
        @grid.reset
    end
  end

  # gosu options
  def needs_cursor?; true end
end


window = MyWindow.new
window.show