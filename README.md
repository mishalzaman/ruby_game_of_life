# Ruby Game of Life

- Ruby 2.5.5
- Gosu 0.14.5

## Setup

Install the gosu gem

`gem install gosu`

Run the application

`ruby main.rbw`

Or you can run the executable in the binary folder. This was created using [Ocra](https://github.com/larsch/ocra).

## About

A Ruby implementation of [John Conway's Game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). Currently work in progress.
It is using the Gosu 2d framework to display the tiles and handle user input.

The Game of Life is a cellular automaton simulation devised by John Conway in 1970. It is a single player game based on a deterministic rule set. The rules are as follow:

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

These rules are applied to each cell within the grid, and their state of being populated (alive), or unpopulated (dead) are processed simulataneously.

## Usage

### Setup

On initial load a grid will be presented. Use the left-lick on your mouse to create patterns, then press [SPACE] to run the simulation. Press [SPACE] again to pause it and add (click on selected cell to remove) new patterns.
You can reset the grid by pressing [R].

Example:

1. Create a pattern

![pattern](pattern_example.png)

2. Press play [SPACE]

![play](play_example.gif)


## Todo

- Add a state machine
- Allow user to set up a grid size

