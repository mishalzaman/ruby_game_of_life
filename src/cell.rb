class Cell
  attr_accessor :current_is_populated, :next_is_populated, :populate, :unpopulate, :seed, :move_to_next_generation, :propagate
  attr_accessor :neighbours

  def initialize
    @current = false
    @next = nil
    @neighbours = []
  end

  def current_is_populated?
    @current
  end

  def next_is_populated?
    @next
  end

  def populate
    @next = true
  end

  def unpopulate
    @next = false
  end

  def seed
    @current = true
  end

  def unseed
    @current = false
  end

  def add_to_neighbours(cell)
    return if cell.class != Cell

    @neighbours << cell
  end

  def populated_neighbours
    live_neighbours = 0
    @neighbours.each do |n|
      live_neighbours += 1 if n.current_is_populated?
    end

    live_neighbours
  end

  # Any live cell with fewer than two live neighbours dies, as if by underpopulation.
  # Any live cell with two or three live neighbours lives on to the next generation.
  # Any live cell with more than three live neighbours dies, as if by overpopulation.
  # Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
  def propagate
    populated_count = populated_neighbours

    if current_is_populated?
      if populated_count < 2 || populated_count > 3
        unpopulate
      else
        populate
      end
    else
      if populated_count == 3
        populate
      else
        unpopulate
      end
    end
  end

  def move_to_next_generation
    puts "next has nil" if @next == nil
    @current = @next
  end
end