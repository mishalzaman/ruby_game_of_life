require_relative '../config.rb'

class Input
  attr_accessor :m_x, :m_y, :update_mouse_position, :calculate_grid, :within_grid

  def initialize
    @m_x = 0
    @m_y = 0
    @grid_x_min = Config::GRID_X_OFFSET
    @grid_x_max = Config::CELL_WIDTH * Config::GRID_WIDTH
    @grid_y_min = Config::GRID_Y_OFFSET
    @grid_y_max = Config::CELL_HEIGHT * Config::GRID_HEIGHT
  end

  def update_mouse_position(x, y)
    @m_x = x
    @m_y = y
  end

  def calculate_grid_row
    ((@m_y - Config::GRID_Y_OFFSET) / Config::CELL_HEIGHT).floor
  end

  def calculate_grid_column
    ((@m_x - Config::GRID_X_OFFSET) / Config::CELL_WIDTH).floor
  end

  def within_grid?
    if @m_x >= @grid_x_min && @m_x <= @grid_x_max && @m_y > @grid_y_min && @m_y <= @grid_y_max
      true
    else
      false
    end
  end
end