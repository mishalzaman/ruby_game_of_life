require_relative 'cell'

class Grid
  attr_accessor :create, :map, :get_neighbours, :history, :reset

  def initialize(width, height)
    @width = width
    @height = height
    @map = []
    @history = []
  end

  def create
    @height.times do
      r = []
      @width.times do r << Cell.new end
      @map << r
    end
  end

  def set_neighbours
    @map.each_with_index do |row, ir|
      row.each_with_index do |column, ic|
        cell = @map[ir][ic]
        # upper left
        if within_bounds?(ir-1, ic-1)
          cell.add_to_neighbours @map[ir-1][ic-1]
        end

        # upper
        if within_bounds?(ir-1, ic)
          cell.add_to_neighbours @map[ir-1][ic]
        end

        # upper right
        if within_bounds?(ir-1, ic+1)
          cell.add_to_neighbours @map[ir-1][ic+1]
        end

        # left
        if within_bounds?(ir, ic-1)
          cell.add_to_neighbours @map[ir][ic-1]
        end
  
        # right
        if within_bounds?(ir, ic+1)
          cell.add_to_neighbours @map[ir][ic+1]
        end

        # lower left
        if within_bounds?(ir+1, ic-1)
          cell.add_to_neighbours @map[ir+1][ic-1]
        end

        # lower
        if within_bounds?(ir+1, ic)
          cell.add_to_neighbours @map[ir+1][ic]
        end

        # lower right
        if within_bounds?(ir+1, ic+1)
          cell.add_to_neighbours @map[ir+1][ic+1]
        end
      end
    end
  end

  def propagate
    add_to_history

    @map.each_with_index do |row, ir|
      row.each_with_index do |column, ic|
        cell = @map[ir][ic]
        cell.propagate
      end
    end
  end

  def add_to_history
    if @history.size > 5
      @history.shift
    end

    map_history = []

    @map.each_with_index do |row, ir|
      history_row = []
      row.each_with_index do |column, ic|
        history_row << @map[ir][ic].current_is_populated?
      end
      map_history << history_row
    end

    @history << map_history
  end

  def shift
    @map.each_with_index do |row, ir|
      row.each_with_index do |column, ic|
        @map[ir][ic].move_to_next_generation
      end
    end
  end

  def seed(row, column)
    return if @map[row].nil?
    return if @map[row][column].nil?

    if @map[row][column].class == Cell
      if @map[row][column].current_is_populated?
        @map[row][column].unseed
      else
        @map[row][column].seed
      end
    end
  end

  def reset
    @map = []
    @history = []
    create
    set_neighbours
  end

  private

  def within_bounds?(row, column)
    if row < 0 || row >= @height || column < 0 || column >= @width
      false
    else
      true
    end
  end
end