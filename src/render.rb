require 'gosu'
require_relative '../config.rb'

module Render
  def self.world(map, history, state)

    map.each_with_index do |row, ir|
      row.each_with_index do |column, ic|
        cell = map[ir][ic]

        x = Config::CELL_HEIGHT * ic + Config::GRID_X_OFFSET
        y = Config::CELL_WIDTH * ir + Config::GRID_Y_OFFSET

        if cell.current_is_populated?
          Gosu::draw_rect(x, y, Config::CELL_WIDTH-1, Config::CELL_WIDTH-1, Config::CURRENT_COLOUR, z = 0, mode = :default)
        else
          if state == :pause
            Gosu::draw_rect(x, y, Config::CELL_WIDTH-1, Config::CELL_WIDTH-1, Gosu::Color::WHITE, z = 0, mode = :default)
          end
        end

        # history
        history.each_with_index do |h,i|
          if h[ir][ic] == true && !Config::HISTORY_COLOURS[i].nil?
            Gosu::draw_rect(x, y, Config::CELL_WIDTH-1, Config::CELL_WIDTH-1, Config::HISTORY_COLOURS[i], z = 0, mode = :default)
          end
        end
      end
    end
  end
end