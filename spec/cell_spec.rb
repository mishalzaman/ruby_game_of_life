require_relative '../src/cell'

describe Cell do
  before do
    @cell = Cell.new
  end

  context '#current_is_populated' do
    it 'checks that the current cell is not populated' do
      expect(@cell.current_is_populated?).to eq false
    end

    it 'checks that the current cell is populated' do
      @cell.seed
      expect(@cell.current_is_populated?).to eq true
    end
  end

  context '#next_is_populated' do
    it 'checks that the next cell is not populated' do
      expect(@cell.next_is_populated?).to eq nil
    end

    it 'checks that the next cell is populated' do
      @cell.populate
      expect(@cell.next_is_populated?).to eq true
    end
  end

  context '#unpopulate' do
    it 'unpopulates the next cell' do
      expect(@cell.next_is_populated?).to eq nil
      @cell.unpopulate
      expect(@cell.next_is_populated?).to eq false
    end
  end

  context '#seed' do
    it 'sets the current cell to true' do
      expect(@cell.current_is_populated?).to eq false
      @cell.seed
      expect(@cell.current_is_populated?).to eq true
    end
  end

  context '#add_to_neighbours' do
    it 'adds a cell to the neighbours array' do
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)

      expect(@cell.neighbours.size).to eq 2
    end

    it 'add an invalid type as cell' do
      @cell.add_to_neighbours("test")

      expect(@cell.neighbours.size).to eq 0
    end
  end

  context '#populated_neighbours' do
    it 'gets a count o the number of 2 populated neighbours' do
      cell1 = Cell.new
      cell1.seed
      cell2 = Cell.new
      cell3 = Cell.new
      cell4 = Cell.new
      cell4.seed
      @cell.add_to_neighbours(cell1)
      @cell.add_to_neighbours(cell2)
      @cell.add_to_neighbours(cell3)
      @cell.add_to_neighbours(cell4)

      expect(@cell.populated_neighbours).to eq 2
    end
  end

  context '#move_to_next_generation' do
    it 'sets a populated next cell to the current' do
      @cell.populate
      @cell.move_to_next_generation
  
      expect(@cell.current_is_populated?).to eq true
    end
  end

  # Any live cell with fewer than two live neighbours dies, as if by underpopulation.
  # Any live cell with two or three live neighbours lives on to the next generation.
  # Any live cell with more than three live neighbours dies, as if by overpopulation.
  # Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
  context '#propagate' do
    before do
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
      @cell.add_to_neighbours(Cell.new)
    end

    it 'propagetes a populate cell with 1 populate neighbour results in an unpopulated next generation' do
      @cell.seed
      @cell.neighbours[0].seed
      @cell.propagate

      expect(@cell.next_is_populated?).to eq false
    end

    it 'propagetes a populate cell with 2 populated neighbour results in an populated next generation' do
      @cell.seed
      @cell.neighbours[0].seed
      @cell.neighbours[1].seed
      @cell.propagate

      expect(@cell.next_is_populated?).to eq true
    end

    it 'propagetes a populate cell with 4 populated neighbour results in an unpopulated next generation' do
      @cell.seed
      @cell.neighbours[0].seed
      @cell.neighbours[1].seed
      @cell.neighbours[6].seed
      @cell.neighbours[7].seed
      @cell.propagate

      expect(@cell.next_is_populated?).to eq false
    end

    it 'propagetes an upopulate cell with 1 populated neighbour results in an unpopulated next generation' do
      @cell.neighbours[0].seed
      @cell.propagate

      expect(@cell.next_is_populated?).to eq false
    end

    it 'propagetes an upopulate cell with 3 populated neighbour results in a populated next generation' do
      @cell.neighbours[0].seed
      @cell.neighbours[1].seed
      @cell.neighbours[2].seed
      @cell.propagate

      expect(@cell.next_is_populated?).to eq true
    end
  end

  context '#move_to_next_generation' do
    it 'sets the next generation to current generation' do
      @cell.populate
      @cell.move_to_next_generation

      expect(@cell.current_is_populated?).to eq true
    end
  end
end