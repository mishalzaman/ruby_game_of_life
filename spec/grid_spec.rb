require_relative '../src/grid'
require_relative '../src/cell'


describe Grid do
  before do
    @grid = Grid.new(20,10)
  end

  context '#create' do
    it 'creates a new empty grid with cells' do
      @grid.create

      expect(@grid.map.first.first.class).to eq Cell
      expect(@grid.map.size).to eq 10
      expect(@grid.map.first.size).to eq 20
    end
  end

  context '#seed' do
    it "sets the cell's current state to true" do
      @grid.create
      @grid.seed(1,1)

      expect(@grid.map[1][1].current_is_populated?).to eq true
      expect(@grid.map[0][0].current_is_populated?).to eq false
    end
  end

  context '#set_neighbours' do
    it 'sets the surrounding neighbours of the cell and returns 8 neighbours' do
      @grid.create
      @grid.set_neighbours

      expect(@grid.map[3][3].neighbours.size).to eq 8
    end

    it 'sets the surrounding neighbours of the cell at 0,0 and returns 3' do
      @grid.create
      @grid.set_neighbours

      expect(@grid.map[0][0].neighbours.size).to eq 3
    end
  end

  # Any live cell with fewer than two live neighbours dies, as if by underpopulation.
  # Any live cell with two or three live neighbours lives on to the next generation.
  # Any live cell with more than three live neighbours dies, as if by overpopulation.
  # Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
  context '#propagate' do
    it 'when the cell is populate and has 1 live neighbour, next generation is unpopulated' do
      @grid.create
      @grid.seed(3,3) # target cell
      @grid.seed(3,2)
      @grid.set_neighbours
      @grid.propagate

      expect(@grid.map[3][3].next_is_populated?).to eq false
    end

    it 'when the cell is populate and has 2 live neighbour, next generation is populated' do
      @grid.create
      @grid.seed(3,3) # target cell
      @grid.seed(3,2)
      @grid.seed(3,4)
      @grid.set_neighbours
      @grid.propagate

      expect(@grid.map[3][3].next_is_populated?).to eq true
    end
  end
end