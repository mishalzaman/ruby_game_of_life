module Config
  # number of cells within the height and width of the grid
  GRID_WIDTH = 126
  GRID_HEIGHT = 66

  # cell width
  CELL_WIDTH = 10
  CELL_HEIGHT = 10

  # grid offset
  GRID_X_OFFSET = 10
  GRID_Y_OFFSET = 10

  # current colour
  CURRENT_COLOUR = Gosu::Color.rgba(125,32,134,255)

  # display history colours
  HISTORY_COLOURS = [
    Gosu::Color.rgba(220,208,229,255),
    Gosu::Color.rgba(200,184,216,255),
    Gosu::Color.rgba(176,146,208,255),
    Gosu::Color.rgba(153,116,191,255),
    Gosu::Color.rgba(129,87,184,255)
  ]
end
